public class Line {
    private Point p1 = null;
    private Point p2 = null;

    Line(double x1, double y1, double x2, double y2) {
        setP1(x1, y1);
        setP2(x2, y2);
    }

    public void setP1(double x1, double y1) { p1 = new Point(x1, y1); }
    public void setP2(double x2, double y2) { p2 = new Point(x2, y2); }

    public Point getP1() { return p1; }
    public Point getP2() { return p2; }
}
