import java.awt.geom.Point2D;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    /**
     * This is point where program starts execution
     * @param args
     * @throws IOException
     */
    public static void main(String[] args) throws IOException {
        // Declaration/Initialization
        DecimalFormat df = new DecimalFormat("#.#"); // Accuracy
        df.setRoundingMode(RoundingMode.HALF_DOWN); // 0.15 = 0.1
        df.setMinimumFractionDigits(0); // One sign after decimical point
        String[] symbols_for_replacing = new String[]{"\\{", ",", "\\}", "\\(", "\\)"}; // Array of symbols which we want to replace
        Polygon my_polygon; // Declaration object of class "Polygon"
        ArrayList<Point> array_of_points = new ArrayList<Point>(); // Dynamic array for points of polygon
        Point point; // Point which is checked on belonging to Polygon
        Scanner sc = new Scanner(new FileReader("input.txt")); // Scanner from file/string
        PrintWriter wr = new PrintWriter("output.txt"); // Printer in file

        // Scan and processing input data
        String coordinates_string = sc.nextLine(); // Coordinates of polygon in string format
        String point_string = sc.nextLine(); // Point in string format
        coordinates_string = deleteSymbols(coordinates_string, symbols_for_replacing, " "); // Deletion
        point_string = deleteSymbols(point_string, symbols_for_replacing, " "); // Deletion
        sc = new Scanner(coordinates_string); // Rediraction scanner from file to string
        while (sc.hasNext()) {
            array_of_points.add(new Point(sc.nextDouble(), sc.nextDouble())); // Read point of polygon from string to "array of points"
        }
        sc = new Scanner(point_string); // Rediraction from one string to other string
        point = new Point(sc.nextDouble(), sc.nextDouble()); // Read point for checking in "point"

        my_polygon = new Polygon(array_of_points); // Initialization "my_polygon"

        // Output result
        if (my_polygon.checkPointOnPolygonBelonging(point)) {
            wr.print("Yes");
        } else wr.print("No");

        wr.print("\n" + df.format(my_polygon.getSquare(0.00001)));

        // Close file
        wr.close();
    }


    /**
     * This method replace all symbols from "symbols" in "string" by "replace"
     * @param string
     * @param symbols
     * @param replace
     * @return
     */
    public static String deleteSymbols(String string, String[] symbols, String replace) {
        for (int i = 0; i < symbols.length; i++) {
            string = string.replaceAll(symbols[i], replace);
        }
        return string;
    }
}
